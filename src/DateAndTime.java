import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateAndTime {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.println("Enter a date in 'dd.MM.yyyy' format to check the leap year:");
        LocalDate date = LocalDate.parse(new Scanner(System.in).nextLine(), formatter);
        System.out.println("Is the year a leap? "+date.isLeapYear());
    }
}
